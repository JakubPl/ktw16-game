package pl.sda;

import pl.sda.model.Event;
import pl.sda.model.Player;
import pl.sda.model.PlotState;
import pl.sda.model.PreConditions;

import java.math.BigDecimal;
import java.util.*;

public class Game {

    private final String playerName;
    private final BigDecimal startingCash;

    public Game(String playerName, BigDecimal startingCash) {
        this.playerName = playerName;
        this.startingCash = startingCash;
    }

    public void play() {
        Scanner scanner = new Scanner(System.in);
        Event healthAddEvent = new Event();
        healthAddEvent.setHealthChange(100);
        healthAddEvent.setEventProbability(90);

        Event healthSubstractEvent = new Event();
        healthSubstractEvent.setHealthChange(-50);

        Event cashAddEvent = new Event();
        cashAddEvent.setCashChange(1000);
        PreConditions cashPreConditions = new PreConditions();
        cashPreConditions.setMinHealth(150);
        cashPreConditions.setMaxHealth(1000);
        cashPreConditions.setMaxCash(BigDecimal.valueOf(100000));
        cashPreConditions.setMinCash(BigDecimal.valueOf(0));
        cashAddEvent.setPreConditions(cashPreConditions);

        final PlotState endState = PlotState.builder()
                .id("I")
                .preChoiceDescription("end")
                .description("That's all")
                //.possibleNextStates(Collections.emptyList())
                .possibleEvents(Collections.singletonList(cashAddEvent))//TODO: to zmien!
                .build();


        final PlotState fighter = PlotState.builder()
                .id("I")
                .preChoiceDescription("Fighter")
                .description("You have selected fighter")
                .possibleEvents(Collections.singletonList(healthAddEvent))
                .possibleNextStates(Collections.singletonList(endState)) //TODO: to zmien!
                .build();


        final PlotState mechant = PlotState.builder()
                .id("I")
                .preChoiceDescription("Merchant")
                .description("You have selected merchant")
                .possibleEvents(Collections.singletonList(healthSubstractEvent))
                .possibleNextStates(Arrays.asList(endState))
                .build();

        final PlotState start = PlotState.builder()
                .id("I")
                .description("Adventure awaits! Please select ship type")
                .possibleNextStates(Arrays.asList(fighter, mechant)) //TODO: to zmien!
                .build();


        final Player player = Player.builder()
                .name(playerName)
                .health(100.0d)
                .cash(startingCash)
                .currentPlotState(start)
                .build();

        while (!player.getCurrentPlotState().isLast()) {
            PlotState currentPlotState = player.getCurrentPlotState();
            printPlayerData(player);

            List<PlotState> currentPossibleStates = currentPlotState.getPossibleNextStates();
            Iterator<PlotState> possibleStatesIterator = currentPossibleStates.iterator();
            for (int i = 0; i < currentPossibleStates.size(); i++) {
                String preChoiceDescription = possibleStatesIterator.next().getPreChoiceDescription();
                System.out.printf("%d. %s%n", i, preChoiceDescription);
            }

            String stringChoice = scanner.nextLine();
            Integer userPlotChoice = Integer.valueOf(stringChoice);
            PlotState plotState = currentPossibleStates.get(userPlotChoice);


            plotState.getPossibleEvents().forEach(event -> {
                int cashChange = event.getCashChange();
                int healthChange = event.getHealthChange();
                int eventProbability = event.getEventProbability();


                PreConditions preConditions = event.getPreConditions();
                if (preConditions != null) {
                    BigDecimal minCash = preConditions.getMinCash();
                    BigDecimal maxCash = preConditions.getMaxCash();
                    double minHealth = preConditions.getMinHealth();
                    double maxHealth = preConditions.getMaxHealth();


                    boolean isHealthInRange = minHealth < player.getHealth()
                            && maxHealth > player.getHealth();
                    boolean isCashInRange = minCash.compareTo(player.getCash()) < 0
                            && maxCash.compareTo(player.getCash()) > 0;
                    if (isHealthInRange && isCashInRange) {


                        handleEvent(player, cashChange, healthChange, eventProbability);
                    }
                } else {
                    handleEvent(player, cashChange, healthChange, eventProbability);
                }
            });
            player.setCurrentPlotState(plotState);
        }
        printPlayerData(player);
    }

    private void handleEvent(Player player, int cashChange, int healthChange, int eventProbability) {
        Random randomGen = new Random();
        int randomNumber = randomGen.nextInt(100);
        if (randomNumber < eventProbability) {
            player.setCash(player.getCash().add(new BigDecimal(cashChange)));
            player.setHealth(player.getHealth() + healthChange);
        }
    }

    private void printPlayerData(Player player) {
        System.out.println("Current HP: " + player.getHealth());
        System.out.println("Current cash balance: " + player.getCash());
        System.out.println(player.getCurrentPlotState().getDescription());
    }

}
