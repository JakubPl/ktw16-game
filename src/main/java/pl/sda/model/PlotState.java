package pl.sda.model;

import java.util.Collections;
import java.util.List;

public class PlotState {
    //unikalny identyfikator stanu
    private String id;
    //opis, ktory powinien zostac wyswietlony po wyborze (np. po wybraniu, ze jest sie kupcem - jestes kupcem!)
    private String description;
    //opis opcji wyboru, np. Wybierz, aby zostac kupcem
    private String preChoiceDescription;
    //mozliwe przejscia z obecnego stanu do nastepnego np. po wybraniu, ze jest sie kupcem mozna wybrac czym handlujemy (np. jedzenie, komputery, bron itd.)
    private List<PlotState> possibleNextStates;
    //zdarzenia zwiazane z wejsciem w stan (np. zmiana zycia, ilosci pieniedzy, dodanie broni)
    private List<Event> possibleEvents;

    private PlotState() {
    }

    public List<Event> getPossibleEvents() {
        return possibleEvents;
    }

    public String getId() {
        return id;
    }

    public String getDescription() {
        return description;
    }

    public List<PlotState> getPossibleNextStates() {
        return possibleNextStates;
    }

    public boolean isLast() {
        return possibleNextStates.isEmpty();
    }

    public static PlotStateBuilder builder() {
        return new PlotStateBuilder();
    }

    public String getPreChoiceDescription() {
        return preChoiceDescription;
    }


    public static final class PlotStateBuilder {
        private String id;
        private String preChoiceDescription;
        private String description;
        private List<PlotState> possibleNextStates = Collections.emptyList();
        private List<Event> possibleEvents = Collections.emptyList();

        private PlotStateBuilder() {
        }

        public PlotStateBuilder id(String id) {
            this.id = id;
            return this;
        }

        public PlotStateBuilder preChoiceDescription(String preChoiceDescription) {
            this.preChoiceDescription = preChoiceDescription;
            return this;
        }

        public PlotStateBuilder description(String description) {
            this.description = description;
            return this;
        }

        public PlotStateBuilder possibleNextStates(List<PlotState> possibleNextStates) {
            this.possibleNextStates = possibleNextStates;
            return this;
        }

        public PlotStateBuilder possibleEvents(List<Event> possibleEvents) {
            this.possibleEvents = possibleEvents;
            return this;
        }

        public PlotState build() {
            PlotState plotState = new PlotState();
            plotState.id = this.id;
            plotState.description = this.description;
            plotState.preChoiceDescription = this.preChoiceDescription;
            plotState.possibleNextStates = this.possibleNextStates;
            plotState.possibleEvents = this.possibleEvents;
            return plotState;
        }
    }
}
