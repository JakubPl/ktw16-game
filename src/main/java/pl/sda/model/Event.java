package pl.sda.model;

import java.util.List;

//klasa reprezentujaca wydarzenie
public class Event {
    //zmiana zycia (moze byc na minus tez)
    private int healthChange;
    //zmiana gotowki (moze byc na minus tez)
    private int cashChange;
    //bron, ktora zostanie dodana
    private List<Weapon> addWeapon;
    //warunki konieczne to spelnienia zadania
    private PreConditions preConditions;
    //prawdopodobienstwo zdarzenia
    private int eventProbability = 100;

    public int getEventProbability() {
        return eventProbability;
    }

    public void setEventProbability(int eventProbability) {
        this.eventProbability = eventProbability;
    }

    public int getHealthChange() {
        return healthChange;
    }

    public void setHealthChange(int healthChange) {
        this.healthChange = healthChange;
    }

    public int getCashChange() {
        return cashChange;
    }

    public void setCashChange(int cashChange) {
        this.cashChange = cashChange;
    }

    public List<Weapon> getAddWeapon() {
        return addWeapon;
    }

    public void setAddWeapon(List<Weapon> addWeapon) {
        this.addWeapon = addWeapon;
    }

    public PreConditions getPreConditions() {
        return preConditions;
    }

    public void setPreConditions(PreConditions preConditions) {
        this.preConditions = preConditions;
    }
}
