package pl.sda.model;

import java.math.BigDecimal;

//warunki wystapienia danego zdarzenia
public class PreConditions {
    //minimalna ilosc gotowki potrzebna do wejscia do PlotState
    private BigDecimal minCash;
    //max ilosc gotowki pozwalajaca wejsc do PlotState
    private BigDecimal maxCash;

    //min ilosc zycia
    private double minHealth;
    //max ilosc zycia
    private double maxHealth;

    //min obrazenia
    private double minDamage;
    //max obrazenia
    private double maxDamage;

    public BigDecimal getMinCash() {
        return minCash;
    }

    public void setMinCash(BigDecimal minCash) {
        this.minCash = minCash;
    }

    public BigDecimal getMaxCash() {
        return maxCash;
    }

    public void setMaxCash(BigDecimal maxCash) {
        this.maxCash = maxCash;
    }

    public double getMinHealth() {
        return minHealth;
    }

    public void setMinHealth(double minHealth) {
        this.minHealth = minHealth;
    }

    public double getMaxHealth() {
        return maxHealth;
    }

    public void setMaxHealth(double maxHealth) {
        this.maxHealth = maxHealth;
    }

    public double getMinDamage() {
        return minDamage;
    }

    public void setMinDamage(double minDamage) {
        this.minDamage = minDamage;
    }

    public double getMaxDamage() {
        return maxDamage;
    }

    public void setMaxDamage(double maxDamage) {
        this.maxDamage = maxDamage;
    }
}
