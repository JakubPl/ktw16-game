package pl.sda;

import java.math.BigDecimal;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Who are you?");
        final String playerName = scanner.nextLine();

        final Game game = new Game(playerName, BigDecimal.valueOf(1000L));
        game.play();
    }
}
